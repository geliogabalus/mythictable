## Thank you for contacting Mythic Table support.

We have recorded your message as Issue %{ISSUE_ID}

We will review your issue and respond as soon as possible.

Mythic Team