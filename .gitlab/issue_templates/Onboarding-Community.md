<!-- Title suggestion: [Onboarding] Newcomer's name -->

# Welcome to Mythic Table's Community Team!

We hope you're as excited as we are to be part of this project! Community is VERY important to us. Please keep in mind that we are all learning, so please ask as many questions as you like!

## 1. Please start out by becoming familiar with the following:

- [ ] [*Main Site*](https://www.mythictable.com/)
- [ ] [*Non-profit Announcement*](https://www.mythictable.com/org/now-non-profit)
- [ ] [*Mission Statement*](https://www.mythictable.com/mission)
- [ ] [*Features*](https://www.mythictable.com/features)
- [ ] [*Wiki*](https://gitlab.com/mythicteam/mythictable/-/wikis/home)
- [ ] [*Gitlab*](https://gitlab.com/mythicteam/mythictable)
- [ ] [*Mythic Table Dev Stories*](https://www.mythictable.com/mythicstories/index)

## 2. Next, please like all of our social media accounts:

- [ ] ![alt text](https://i.imgur.com/TtIyDnO.png "Youtube") - [**Youtube**](https://www.youtube.com/mythictable)
- [ ] ![alt text](https://i.imgur.com/RBSCNgo.png "Facebook") - [**Facebook**](https://www.facebook.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/M9RR4Hf.png "Instagram") - [**Instagram**](https://www.instagram.com/mythic_table/)
- [ ] ![alt text](https://i.imgur.com/hgpY9gu.png "Reddit") - [**Reddit**](https://www.reddit.com/r/mythictable/)
- [ ] ![alt text](https://i.imgur.com/rEAMp9o.png "Twitter") - [**Twitter**](https://twitter.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/kX34PqK.png "Patreon") - [**Patreon**](https://www.patreon.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/m9k88St.png "Tumblr") - [**Tumblr**](https://mythictable.tumblr.com/)
- [ ] ![alt text](https://i.imgur.com/EbNeByg.png "Product Hunt") - [**Product Hunt**](https://www.producthunt.com/posts/mythic-table/)

## 3. Lastpass

- [ ] [Create a LastPass Account](https://lastpass.com/create_account.php?)

## 4. Join the following channels on Slack:

* [ ] #community
* [ ] #community-outreach
* [ ] #content
* [ ] #design
* [ ] #mythic-table
* [ ] #dev (It is for Programmers/Developers but it is good to listen in on)
* [ ] #random
* [ ] **Join #coffee-break**! We use this for our donut channel social meetups.  Get to know the rest of the team!

#### Optional:

- [ ] get Slack on your phone
- [ ] update your Slack settings to customize your notifications.

## 5. Tell the world!

- [ ] **Join us on Linkedin** https://www.linkedin.com/company/mythic-table/

## 6. We will share our Community and Social Media Documentation with you, as well as making you an admin on our Facebook page! 

* [ ] Done 

Thanks for reading this ticket! If you notice something that should be added please let me know.
*Sarah Kilby, Director of Communications
Mythic Table Foundation*
****

/label ~"type::onboarding"
