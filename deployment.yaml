apiVersion: batch/v1
kind: Job
metadata:
  name: migrator
spec:
  template:
    spec:
      initContainers:
        - name: init-mongodb-allowlist
          image: joselbonilla/mongocli-docker
          env:
            - name: MCLI_PUBLIC_API_KEY
              valueFrom: 
                secretKeyRef:
                  name: mongodb-api
                  key: public-mongodb-api-key
            - name: MCLI_PRIVATE_API_KEY
              valueFrom: 
                secretKeyRef:
                  name: mongodb-api
                  key: private-mongodb-api-key
            - name: MCLI_ORG_ID
              value: 5e5af965a7b6f61bf0f40dcc
            - name: MCLI_PROJECT_ID
              value: 5e5af965a7b6f61bf0f40dd1
            - name: MCLI_SERVICE
              value: cloud
      containers:
      - name: migrator
        image: registry.gitlab.com/mythicteam/mythictable/migrator:latest
        command: ["dotnet", "/app/Migrator.dll", "$(MTT_MONGODB_CONNECTIONSTRING)", "$(MTT_MONGODB_DATABASENAME)"]  # There is a missing final argument here that will allow a version for backoffs
        resources:
        env:
          - name: MTT_MONGODB_CONNECTIONSTRING
            valueFrom:
              secretKeyRef:
                name: mongodb-settings
                key: connectionstring
          - name: MTT_MONGODB_DATABASENAME
            valueFrom:
              secretKeyRef:
                name: mongodb-settings
                key: database
      restartPolicy: Never
  backoffLimit: 2
  ttlSecondsAfterFinished: 1
status:
  succeeded: 0
---
apiVersion: v1
kind: Service
metadata:
  name: mythic-table-service
  labels:
    app: mythic-table
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: mythic-table
---
apiVersion: v1
kind: Service
metadata:
  name: mythictable-nodeport
spec:
  externalTrafficPolicy: Cluster
  ports:
  - port: 8080
    protocol: TCP
    targetPort: 80
  selector:
    app: mythic-table
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    app.gitlab.com/env: <CI_ENVIRONMENT_SLUG>
    app.gitlab.com/app: <CI_PROJECT_PATH_SLUG>
  name: mythic-table
  labels:
    app: mythic-table
spec:
  replicas: 3
  selector:
    matchLabels:
      app: mythic-table
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 33%
  template:
    metadata:
      labels:
        app: mythic-table
    spec:
      initContainers:
        - name: init-mongodb-allowlist
          image: joselbonilla/mongocli-docker
          env:
            - name: MCLI_PUBLIC_API_KEY
              valueFrom: 
                secretKeyRef:
                  name: mongodb-api
                  key: public-mongodb-api-key
            - name: MCLI_PRIVATE_API_KEY
              valueFrom: 
                secretKeyRef:
                  name: mongodb-api
                  key: private-mongodb-api-key
            - name: MCLI_ORG_ID
              value: 5e5af965a7b6f61bf0f40dcc
            - name: MCLI_PROJECT_ID
              value: 5e5af965a7b6f61bf0f40dd1
            - name: MCLI_SERVICE
              value: cloud
      serviceAccount: mythic-table-api-edge-ksa
      containers:
        - name: mythic-table
          image: registry.gitlab.com/mythicteam/mythictable:<VERSION>
          ports:
            - containerPort: 80
          env:
            - name: MTT_MONGODB_CONNECTIONSTRING
              valueFrom:
                secretKeyRef:
                  name: mongodb-settings
                  key: connectionstring
            - name: MTT_MONGODB_DATABASENAME
              valueFrom:
                secretKeyRef:
                  name: mongodb-settings
                  key: database
            - name: MTT_USE_GCP_IMAGE_STORE
              value: "true"
            - name: MTT_GCP_BUCKET_IMAGES
              value: "mythic-table-image-store-<CI_SCOPE>"
            - name: MTT_AUTHORITY
              value: "https://<KEY_DOMAIN>/auth/realms/MythicTable"
            - name: MTT_REDIS_CONN_STRING
              value: "redis-backplane"
          livenessProbe:
            tcpSocket:
              port: 80
          readinessProbe:
            httpGet:
              path: /healthz
              port: 80
            initialDelaySeconds: 2
            periodSeconds: 2
---  
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.org/websocket-services: "ws-svc"
    nginx.com/sticky-cookie-services: "serviceName=mythic-table-service sticky-table path=/api/live"
    nginx.ingress.kubernetes.io/affinity: cookie
    nginx.ingress.kubernetes.io/affinity-mode: persistent
    nginx.ingress.kubernetes.io/proxy-body-size: 32m
    certmanager.k8s.io/cluster-issuer: letsencrypt-prod
  name: <CI_SCOPE>
spec:
  rules:
    - host: <CI_DOMAIN>
      http:
        paths:
          - backend:
              serviceName: mythic-table-service
              servicePort: 80
            path: /
  tls:
      - hosts:
        - <CI_DOMAIN>
        secretName: mythictable-tls
---
apiVersion: v1
kind: Service
metadata:
  name: redis-backplane
  labels:
    app: redis
    role: backplane
    tier: backend
spec:
  ports:
  - port: 6379
    targetPort: 6379
  selector:
    app: redis
    role: backplane
    tier: backend
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-backplane
  labels:
    app: redis
spec:
  selector:
    matchLabels:
      app: redis
      role: backplane
      tier: backend
  replicas: 1
  template:
    metadata:
      labels:
        app: redis
        role: backplane
        tier: backend
    spec:
      containers:
      - name: backplane
        image: redis:alpine
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 6379
