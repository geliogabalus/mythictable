﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MythicTable.Files.Controllers;
using MythicTable.Files.Data;
using Xunit;

namespace MythicTable.Tests.Files.Controllers
{
    class FileControllerTestHelper
    {
        public static async Task<FileDto> PostFile(FormFileCollection fileCollection, FileController controller)
        {
            var actionResult = await controller.PostFile(fileCollection) as ActionResult<UploadResult>;
            var okResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var results = Assert.IsType<UploadResult>(okResult.Value);
            Assert.Equal(1, results.Count);
            var firstFile = results.Files.Single();
            return firstFile;
        }
    }
}
