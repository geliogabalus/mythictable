using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Integration.TestUtils.Util;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.API
{
    public class CollectionApiTests
    {
        private readonly HttpClient client;

        public CollectionApiTests()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            client = server.CreateClient();
        }

        [Fact]
        public async Task GetCollectionReturnsEmpty()
        {
            await ProfileTestUtil.Login(client);

            using var response = await RequestHelper.GetStreamAsync(client, "/api/collections/test");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            var jObjects = JsonConvert.DeserializeObject<List<JObject>>(json);
            Assert.Empty(jObjects);
        }

        [Fact]
        public async Task PostAndGetReturnsJObjects()
        {
            await ProfileTestUtil.Login(client);

            var o = new JObject
            {
                { "Name", "Integration Test JObject" }
            };
            var collection = "test";

            await CreateObjectInCollection(collection, o);

            List<JObject> jObjects = await GetCollection(collection);
            Assert.Single(jObjects);
            Assert.Equal("Integration Test JObject", jObjects[0]["Name"]);
        }

        [Fact]
        public async Task UpdateChangesJObjects()
        {
            await ProfileTestUtil.Login(client);

            var o = new JObject
            {
                { "Name", "Integration Test JObject" }
            };
            const string collection = "test";

            var jObject = await CreateObjectInCollection(collection, o);

            var patch = new JsonPatchDocument()
                            .Replace("Name", "Update Test JObject")
                            .Add("foo", "bar");
            jObject = await UpdateObjectInCollection(collection, patch, jObject["_id"]?.ToString());

            Assert.Equal("Update Test JObject", jObject["Name"]);
            Assert.Equal("bar", jObject["foo"]);
        }

        [Fact]
        public async Task DeleteApi()
        {
            await ProfileTestUtil.Login(client);

            var collection = "test";
            var jObject = await CreateObjectInCollection(collection, new JObject());

            await DeleteObjectInCollection(collection, jObject["_id"]?.ToString());

            List<JObject> jObjects = await GetCollection(collection);
            Assert.Empty(jObjects);
        }

        private async Task<JObject> CreateObjectInCollection(string collection, JObject o)
        {
            using var response = await RequestHelper.PostStreamAsync(client, $"/api/collections/{collection}", o);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(json);
        }

        private async Task<List<JObject>> GetCollection(string collection)
        {
            using var response = await RequestHelper.GetStreamAsync(client, $"/api/collections/{collection}");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<JObject>>(json);
        }

        private async Task<JObject> UpdateObjectInCollection(string collection, JsonPatchDocument patch, string id)
        {
            using var response = await RequestHelper.PutStreamAsync(client, $"/api/collections/{collection}/id/{id}", patch);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(json);
        }

        private async Task DeleteObjectInCollection(string collection, string id)
        {
            using var response = await RequestHelper.DeleteStreamAsync(client, $"/api/collections/{collection}/id/{id}");
            response.EnsureSuccessStatusCode();
        }
    }
}
