using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Util;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Campaign
{
    public class MultiUserCampaignTests
    {
        [Fact]
        public async Task UnauthorizedCampaignDeleteReturns401()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            var client = server.CreateClient();

            await ProfileTestUtil.Login(client);
            
            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();
            var campaignObject = JsonConvert.DeserializeObject<CampaignDTO>(json);

            var res = await RequestHelper.CreateUser(server, "fake-user-02");
            res.EnsureSuccessStatusCode();

            var request = server.CreateRequest($"/api/campaigns/{campaignObject.Id}");
            request.AddHeader(TestStartup.FAKE_USER_ID_HEADER, "fake-user-02");

            using var deleteResponse = await request.SendAsync("DELETE");
            Assert.Equal(HttpStatusCode.Unauthorized, deleteResponse.StatusCode);
        }
    }
}