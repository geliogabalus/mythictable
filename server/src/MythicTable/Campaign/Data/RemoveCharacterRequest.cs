namespace MythicTable.Campaign.Data
{
    public class RemoveCharacterRequest
    {
        public string CampaignId { get; set; }
        public string CharacterId { get; set; }
    }
}