﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MythicTable.Files.Controllers;
using MythicTable.Files.Exceptions;

namespace MythicTable.Files.Data
{
    public class InMemoryFileOwnershipProvider : IFileOwnershipProvider
    {
        private long nextId = 1;

        private readonly Dictionary<string, Dictionary<long, FileDto>> userLists =
            new Dictionary<string, Dictionary<long, FileDto>>();

        public async Task<FileDto> Delete(string id, string userId)
        {
            var dto = await Get(id, userId);
            var userFiles = userLists.GetValueOrDefault(userId);
            userFiles?.Remove(long.Parse(id));
            return dto;
        }

        public Task<FileDto> Get(string id, string userId)
        {
            var userFiles = userLists.GetValueOrDefault(userId);
            if (userFiles == null)
            {
                throw new FileStorageException($"Could not find File of Id: '{id}'", HttpStatusCode.NotFound);
            }
            var longId = long.Parse(id);
            var dto = userFiles.GetValueOrDefault(longId);
            if (dto == null)
            {
                throw new FileStorageException($"Could not find File of Id: '{id}'", HttpStatusCode.NotFound);
            }
            if (dto.User != userId)
            {
                throw new FileStorageException($"File '{id}' does not belong to user '{userId}'",
                    HttpStatusCode.Forbidden);
            }
            return Task.FromResult(dto);
        }

        public Task<List<FileDto>> GetAll(string userId)
        {
            var userFiles = userLists.GetValueOrDefault(userId);
            if (userFiles == null)
            {
                return Task.FromResult(new List<FileDto>());
            }
            return Task.FromResult(new List<FileDto>(userFiles.Values));
        }

        public Task<List<FileDto>> Filter(string userId, FileFilter filter)
        {
            var userFiles = userLists.GetValueOrDefault(userId);
            if (userFiles == null) return Task.FromResult(new List<FileDto>());
            if (filter?.Path == null || filter.Path == string.Empty)
            {
                return Task.FromResult(new List<FileDto>(userFiles.Values));
            }
            return Task.FromResult(userFiles.Values.Where(file => file.Path == filter.Path).ToList());
        }

        public Task<FileDto> Create(FileDto dto)
        {
            var userFiles = userLists.GetValueOrDefault(dto.User) ?? (userLists[dto.User] = new Dictionary<long, FileDto>());
            userFiles[nextId] = dto;
            dto.Id = nextId.ToString();
            nextId++;
            return Task.FromResult(dto);
        }

        public Task<FileDto> FindDuplicate(string userId, string md5)
        {
            var userFiles = userLists.GetValueOrDefault(userId);
            return userFiles != null
                ? Task.FromResult(userFiles.Values.FirstOrDefault(dto => dto.Md5 == md5))
                : Task.FromResult<FileDto>(null);
        }
    }
}